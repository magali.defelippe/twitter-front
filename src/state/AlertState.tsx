import React,{FC, useContext, useState,useMemo, useCallback} from 'react';
import { Color } from "@material-ui/lab";

type AlertState = {
  text:string,
  type: Color,
  open:boolean,
}
type AlertContext = {
  alertState: AlertState,
  sendAlert: (text:string, type?: Color) => void,
  closeAlert: () => void
}
const initialState:AlertState = {
  text: "",
  type: "success",
  open: false,
}

const initialValue:AlertContext = {
  alertState: initialState,
  sendAlert: () => {},
  closeAlert: () => {}
}

const AlertContext = React.createContext(initialValue);



const AlertProvider: FC<{children:any}>= ({children})=>{
  const [alertState, setAlertState] = useState<AlertState>(initialState) 

  const sendAlert = useCallback(
    (text:string, type: Color = "success") => {
      setAlertState({
        text,
        type,
        open: true,
      })
    },
    [setAlertState],
  )
  const closeAlert = useCallback(
    () => { setAlertState(initialState) },
    [setAlertState],
  )
  const value = useMemo(() => ({
    alertState,
    sendAlert,
    closeAlert,
  }),[alertState, sendAlert, closeAlert,])

  return <AlertContext.Provider value={value}>{children}
</AlertContext.Provider>
};

export const useAlert = () => {
  return useContext(AlertContext);
}

export default AlertProvider
