import { Color } from '@material-ui/lab';

export interface AlertUser{
    text: string,
    session: boolean,
    type: Color,
    open: boolean
}
export interface ChangesAlert{
    text: string,
    type: Color,
    open: boolean
}

export interface PropsAlert {
    openAlert: (alert: ChangesAlert) => void;
}

export function createUserAlert(text: string, session: boolean, type: Color){
    const alert:AlertUser = {
        text,
        session,
        type,
        open:true
    };
    return alert;
}

export function createAlert(text: string, type: Color){
    const alert:ChangesAlert = {
        text,
        type,
        open: true
    };
    return alert;
}