import axios from 'axios';

async function getFollowers(nickname: string) {
    try {
        const followers = await axios.get(`http://localhost:3333/follow/followers/${nickname}`)
        return followers.data;
    } catch (error) {
        console.log(error)
    }
}

export default getFollowers;