import axios from 'axios';

async function getFollowings(nickname: string) {
    try {
        const followings = await axios.get(`http://localhost:3333/follow/followings/${nickname}`)
        return followings.data;
    } catch (error) {
        console.log(error)
    }
}

export default getFollowings;