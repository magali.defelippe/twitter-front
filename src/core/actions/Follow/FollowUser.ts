import axios from 'axios';
import { Follow } from '../../domain/Follow/Follow';

export default async function followUser(follower: string, following: string) {

    try {
        const body: Follow = {
            follower,
            following
        }

        const response = await axios.post("http://localhost:3333/follow", body)

        return response.data;

    } catch (error) {
        throw error.response.data;
    }
}