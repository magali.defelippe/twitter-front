import axios from 'axios';
import {User} from '../../domain/User/User';

export async function registerUser(name: string, nickname: string) {

    try {
        const body: User = {
            name,
            nickname
        }
        const response = await axios.post("http://localhost:3333/users", body)

        return {data: response.data, auth: true}

    } catch (error) {
       throw error.response.data
    }
}