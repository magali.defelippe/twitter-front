import axios from 'axios';

interface NewName{
    newName: string
};

async function changeUserName(newName: string, nickname: string) {

    try {
        const body: NewName = {
            newName
        }

        const response = await axios.put(`http://localhost:3333/users/${nickname}`, body)

        return response.data;

    } catch (error) {
        throw error.response.data;
    }
}

export default changeUserName