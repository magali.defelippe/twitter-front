import axios from 'axios';

async function getUserInformation(nickname: string) {
    try {
        const response = await axios.get(`http://localhost:3333/users/${nickname}`);
        return response;
    } catch (error) {
      console.log(error)
    }
}

export default getUserInformation;