import axios from 'axios';

export async function getTweets( nickname: string) {
    try {
        const response = await axios.get(`http://localhost:3333/tweets/${nickname}`)

        return response.data;
    } catch (error) {
        console.log(error)
    }
};