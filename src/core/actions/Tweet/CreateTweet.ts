import axios from 'axios';
import {Tweet} from '../../domain/Tweet/Tweet';

export async function createTweet(content: string, nickname: string) {

    try {
        const body: Tweet = {
            content
        };

        const response = await axios.post(`http://localhost:3333/tweets/${nickname}`, body)

        return response.data;

    } catch (error) {
        throw error.response.data;
    }
}