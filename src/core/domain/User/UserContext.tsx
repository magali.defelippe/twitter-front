import React,{FC, useContext, useState,useMemo} from 'react';

const UserContext = React.createContext({
  nickname: '',
  setNickname: (nickname: string) => {},
  name: '',
  setName: (name: string) => {},
  followings: [""],
  setFollowings: (followings: string[]) => {},
  followers: [""],
  setFollowers: (followers: string[]) => {},
  auth: false,
  setAuth: (auth: boolean) => {},
  tweets: [""],
  setTweets: (tweets: string[]) => {}
});



const UserContextProvider: FC<{children:any}>= ({children})=>{
  const [nickname, setNickname] = useState("");
  const [name, setName] = useState("");
  const [followings, setFollowings] = useState<string[]>([]);
  const [followers, setFollowers] = useState<string[]>([]);
  const [auth, setAuth] = useState(false);
  const [tweets, setTweets] = useState([""]);

const value = useMemo(() => ({
  nickname,
    setNickname,
    name,
    setName,
    followings,
    setFollowings,
    followers,
    setFollowers,
    auth,
    setAuth,
    tweets,
    setTweets
}),[nickname,
  setNickname,
  name,
  setName,
  followings,
  setFollowings,
  followers,
  setFollowers,
  auth,
  setAuth,
  tweets,
  setTweets])

  return <UserContext.Provider  value={value}>{children}
</UserContext.Provider>
};

export const useUserContext = () => {
  return useContext(UserContext);
}

export default UserContextProvider
