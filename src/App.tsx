import React from "react";
import UserContextProvider,{useUserContext} from "./core/domain/User/UserContext";
import { HashRouter, Switch, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Error from "./pages/Error/Error";
import { ThemeProvider } from "@material-ui/styles";
import theme from "./theme/Theme";
import AlertProvider from './state/AlertState';
function App() {

  return (
    <HashRouter>
      <div>
        <AlertProvider>  
        <Switch>
          <UserContextProvider>
            <ThemeProvider theme={theme}>
         
              <Routes/>
            </ThemeProvider>
          </UserContextProvider>
        </Switch>
        </AlertProvider>
      </div>
    </HashRouter>
  );
}

const Routes = () => {
  const {auth} = useUserContext();
  return (
  <React.Fragment>
    <Route path="/" exact component={Login} />
    {auth ? 
    (
      <Route path="/home" exact component={Home} />
    ) 
    : 
    (
      <Route path="/home" exact component={Error} />
  
    )}
  </React.Fragment>
  )
}
export default App;
