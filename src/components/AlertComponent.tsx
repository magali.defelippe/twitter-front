import { Alert } from "@material-ui/lab";
import React from "react";
import { useAlert } from "../state/AlertState";

const AlertComponent = () => {
  const { alertState, closeAlert } = useAlert();
  console.log(alertState);
  return (
    <div>
      {alertState.open ? (
        <Alert onClose={closeAlert} severity={alertState.type}>
          {alertState.text}
        </Alert>
      ) : null}
    </div>
  );
};

export default AlertComponent;
