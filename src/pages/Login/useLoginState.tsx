import { useEffect, useState } from "react";
import {useUserContext} from "../../core/domain/User/UserContext";
import { useAlert } from "../../state/AlertState";
import useRegisterUser from './useRegisterUser';
import { useHistory } from "react-router-dom";

export const useLoginState = () => { 
    const [checkbox, setCheckbox] = useState(false);
    const [name, setSessionName] = useState("");
    const [nickname, setSessionNickname] = useState("");

    const { sendAlert} = useAlert();
    const { setNickname, setAuth } = useUserContext();
    const {execute, data, auth, error} = useRegisterUser();
    const history = useHistory();
    
    function send(){
        execute(name, nickname);
    }

    useEffect(()=>{
        if(auth || error === "El nickname ya está en uso"){
            setNickname(nickname)
            setAuth(true)
            history.push("/home");
        }else if(error){
            sendAlert(error, 'error');
        }
    }, [auth, error])
   
    return {
        checkbox,
        setCheckbox,
        setSessionName,
        setSessionNickname,
        send
      };
}