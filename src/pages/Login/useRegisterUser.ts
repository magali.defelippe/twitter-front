import { useState } from "react";
import { registerUser } from "../../core/actions/User/RegisterUser";


const useGetUserInformation = () => {
    const [data, setData] = useState("");
    const [auth, setAuth] = useState(false);
    const [error, setError] = useState("");

    const execute = async (name: string, nickname: string) => {
        try {
            const response = await registerUser(name, nickname);
            setData(response.data);
            setAuth(response.auth);
        }
        catch (error) {
            setError(error)
        }


    }
    return { execute, data, auth, error };
}

export default useGetUserInformation;