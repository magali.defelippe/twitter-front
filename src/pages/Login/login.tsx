import {
  Grid,
  CardContent,
  Button,
  Checkbox,
  Typography,
  Divider,
  TextField,
  FormControlLabel,
  makeStyles,
  Box,
} from "@material-ui/core";
import AlertComponent from '../../components/AlertComponent'
import {useLoginState} from './useLoginState';

export const Login = () => {
  const {checkbox,
    setCheckbox,
    setSessionName,
    setSessionNickname,
    send} = useLoginState();

  const useStyles = makeStyles((theme) => ({
    text: {
      color: "rgba(0, 0, 0, 0.54)",
    },
    formContainer: {
      borderRadius: "10px",
      padding: "2%",
      paddingTop: "5%",
      position: 'relative',
      top: '-50%'
    },
    grid: {
      minHeight: "100vh",
      backgroundColor: "#fafafa",
    },
    title: {
      marginBottom: "10px",
      textAlign: "center",
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="center"
        className={classes.grid}
      > 
      <div>
        <AlertComponent/>
      </div>
        <Grid container justify="center" style={{ position: "absolute", top: '20%' }}  alignItems="center" alignContent="center">
          
          <Box className={classes.formContainer} boxShadow={1}>
            <Typography variant="h4" className={classes.title}>
              Sign in
            </Typography>
            <Divider variant="middle" />
            <form noValidate autoComplete="off" onSubmit={send}>
              <CardContent>
                <Grid item xs>
                  <TextField
                    id="outlined-disabled"
                    label="Nombre"
                    placeholder="Escribe tu nombre"
                    onChange={(e) => setSessionName(e.target.value)}
                    fullWidth
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    margin="normal"
                    value={undefined}
                  />
                </Grid>

                <Grid item xs>
                  <TextField
                    id="outlined-disabled"
                    label="Nickname"
                    placeholder="Escribe tu Nickname"
                    onChange={(e) => setSessionNickname(e.target.value)}
                    fullWidth
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    margin="normal"
                    value={undefined}
                  />
                </Grid>
                <Grid container justify="space-between">
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={checkbox}
                        onChange={(e) => setCheckbox(e.target.checked)}
                        name="checkedB"
                        color="primary"
                      />
                    }
                    label="Remember me"
                    className={classes.text}
                  />

                  <Button color="primary">
                    <span className={classes.text}>Forgot password?</span>
                  </Button>
                </Grid>
                <Button
                  variant="contained"
                  color="primary"
                  fullWidth
                  type="submit"
                  size="large"
                >
                  Login
                </Button>
              </CardContent>
            </form>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Login;
