import {
  Box,
  TextareaAutosize,
  Card,
  Button,
  Divider,
  Grid,
  Typography,
} from "@material-ui/core";
import {useTweetState} from '../state/useTweetState';

const WriteTweet = () => {

  const {setContent, sendTweet, content} = useTweetState();

  return (
    <Box padding="16px">
      <Typography>Escribe un tweet</Typography>
      <Card variant="outlined">
        <TextareaAutosize
          value={content}
          onChange={(e) => setContent(e.target.value)}
          style={{ border: "none", width: "100%", minHeight: "10%" }}
        ></TextareaAutosize>
        <Divider />
        <Grid container justify="flex-end">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            size="small"
            style={{ margin: "10px" }}
            onClick={(e) => sendTweet()}
          >
            Enviar
          </Button>
        </Grid>
      </Card>
    </Box>
  );
};

export default WriteTweet;
