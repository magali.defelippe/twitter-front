import {
  InputBase,
  IconButton,
  Grid,
  Card,
  CardContent,
  Typography,
  Button,
  Avatar,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { useSearchState } from "../state/useSearchState";

const SearchUsersContainer = () => {
  const { searchValue, setSearchValue, searchUser, userFind, followAnUser} = useSearchState();

  return (
    <Grid>
      <Grid>
        <Card variant="outlined" style={{ backgroundColor: "white" }}>
          <CardContent style={{ padding: "5px" }}>
            <InputBase
              style={{ width: "80%" }}
              placeholder="Buscar usuario"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
            />
            <IconButton
              onClick={() => searchUser()}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
          </CardContent>
        </Card>
      </Grid>
      <Grid style={{ marginTop: "40px" }}>
        {userFind.nickname ? (
          <Card variant="outlined" style={{ margin: "16px" }}>
            <CardContent style={{paddingBottom: '16px'}}>
              <Grid container justify="space-around" style={{marginBottom: '16px'}}>
                <Grid item>
                  <Avatar></Avatar>
                </Grid>
                <Grid item>
                  <Typography>{userFind.nickname}</Typography>
                  <Typography>{userFind.name}</Typography>
                </Grid>
              </Grid>
              <Grid container justify="center">
                {userFind.follow ? (
                  <Typography color="primary">Siguiendo</Typography>
                ) : (
                  <Button fullWidth color="primary" variant="contained" onClick={() => followAnUser()}>
                    Seguir
                  </Button>
                )}
              </Grid>
            </CardContent>
          </Card>
        ) : null}
      </Grid>
    </Grid>
  );
};

export default SearchUsersContainer;
