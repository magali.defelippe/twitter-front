import {
  Button,
  Avatar,
  Divider,
  CardContent,
  Typography,
  Grid,
  TextField,
  IconButton,
} from "@material-ui/core";
import { Edit, Cancel, CheckCircle } from "@material-ui/icons";
import { useContext, useEffect, useState } from "react";
import {useUserInformationState} from "../state/useUserInformationState";

import UserFollow from "./UserFollow";


const UserInformation = () => {
  const {
    name,
    nickname,
    newName,
    follow,
    followings,
    followers,
    open,
    title,
    edit,
    openFollow,
    handleClose,
    changeName,
    changeNameByInput,
    setEditValue,
    loadUserInformation
  } = useUserInformationState();

  useEffect(()=>{
    loadUserInformation()
  }, [])

  return (
    <CardContent>
      <Grid container justify="center">
        <Avatar>{nickname.charAt(1)}</Avatar>
      </Grid>
        <div>
        {edit ? (
           <Grid container justify="space-between">
            <IconButton style={{color: "#d50000"}} onClick={() => setEditValue(false)}>
              <Cancel />
            </IconButton>
            <TextField
              size="small"
              id="outlined-disabled"
              label="Nombre"
              placeholder="Escribe tu Nickname"
              onChange={(e) => changeNameByInput(e.target.value)}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
              value={newName}
            />
              <IconButton style={{color: "#4caf50"}} onClick={()=> changeName()}>
              <CheckCircle />
            </IconButton>
          </Grid>
        ) : (
          <Grid container justify="center">
            <IconButton color="primary" onClick={() => setEditValue(true)}>
              <Edit />
            </IconButton>
            <Typography style={{lineHeight: '3'}}>{name}</Typography>
          </Grid>
        )}
        </div>
      <Grid container justify="center">
        <Typography>{nickname}</Typography>
      </Grid>
      <Grid container justify="space-between">
        <Grid>
          <Button
            onClick={(e) => openFollow(followings, "Seguidos")}
            color="primary"
          >
            {followings.length + " Siguiendo"}
          </Button>
        </Grid>
        <Grid>
          <Button
            onClick={(e) => openFollow(followers, "Seguidores")}
            color="primary"
          >
            {followers.length + " Seguidores"}
          </Button>
        </Grid>
      </Grid>
      <Divider />
      <UserFollow
        title={title}
        onClose={handleClose}
        open={open}
        follows={follow}
      />
    </CardContent>
  );
};

export default UserInformation;
