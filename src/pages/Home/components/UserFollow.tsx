import {
  Dialog,
  DialogTitle,
  Typography,
  Divider
} from "@material-ui/core";

interface userFollowProps {
  open: boolean;
  follows: string[];
  title: string;
  onClose: () => void;
}

const UserFollow = (props: userFollowProps) => {
  function handleClose() {
    props.onClose();
  }
  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={props.open}
      fullWidth={true}
      maxWidth="xs"
    >
      <DialogTitle>{props.title}</DialogTitle>
      <div>
        {props.follows.map((follow) => (
          <div key={follow}>
            <Divider />
            <Typography align="center" style={{ margin: "5px" }}>
              {follow}
            </Typography>
          </div>
        ))}
      </div>
    </Dialog>
  );
};

export default UserFollow;
