import {
  AppBar,
  Toolbar,
  Typography,
} from "@material-ui/core";

const Nav = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" noWrap>
          Kata-twitter
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
