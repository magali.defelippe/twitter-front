import {
  Card,
  Grid,
  Typography,
} from "@material-ui/core";
import { useTweetState } from "../state/useTweetState";

const TweetsContainer = () => {
  const { tweets } = useTweetState();

  return (
    <Grid container justify="center" style={{ marginTop: "50px" }}>
      {tweets.length === 0 ? (
        <Typography variant="h5">No hay tweets...</Typography>
      ) : (
        tweets.map((tweet, index) => (
          <Card
            variant="outlined"
            style={{ width: "50%", marginTop: "16px", marginBottom: "16px" }}
            key={tweet+index}
          >
            <Typography align="center" style={{ margin: "5px" }}>
              {tweet}
            </Typography>
          </Card>
        ))
      )}
    </Grid>
  );
};

export default TweetsContainer;
