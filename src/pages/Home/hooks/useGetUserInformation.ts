import { useState } from "react";
import getUserinformation from "../../../core/actions/User/FindUser";
import {User} from "../../../core/domain/User/User";


const useGetUserInformation = () => {
    const [data, setData] = useState<User>({name: "", nickname: ""});

    const execute = async (nickname: string) => {
        const response = await getUserinformation(nickname);
        setData(response?.data);
    }
    return {execute, data};
}

export default useGetUserInformation;