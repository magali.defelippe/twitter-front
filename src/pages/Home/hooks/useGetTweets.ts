import {getTweets} from "../../../core/actions/Tweet/GetTweets";
import { useState } from "react";

const useGetTweets = () => {
  const [data, setData] = useState([])

    const execute = async (nickname: string) => { 
        const response = await getTweets(nickname);
        setData(response);
    }
    return {execute, data};
}

export default useGetTweets;