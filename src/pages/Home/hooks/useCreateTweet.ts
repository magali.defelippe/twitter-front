import {createTweet} from "../../../core/actions/Tweet/CreateTweet";
import { useState } from "react";

const useCreateTweet = () => {
    const [data, setData] = useState("")
    const [error, setError] = useState("")
  
      const execute = async (content: string,nickname: string) => {
        setData("") 
        try {
        const response = await createTweet(content, nickname);
          setData(response);
        } catch (error) {
            setError(error)
        }
  
      }
      return {execute, data, error};
}

export default useCreateTweet;