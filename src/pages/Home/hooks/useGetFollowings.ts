import { useState } from "react";
import getUserFollowings from "../../../core/actions/Follow/GetUserFollowings";

const useGetFollowings = () => {
    const [data, setData] = useState<string[]>([]);

    const execute = async (nickname: string) => {
        const response = await getUserFollowings(nickname);
        setData(response);
    }
    return {execute, data};
}

export default useGetFollowings;