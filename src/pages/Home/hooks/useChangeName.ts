import { useState } from "react";
import changeUserName from "../../../core/actions/User/ChangeName";

const useChangeName = () => {
  const [data, setData] = useState("")
  const [error, setError] = useState("")

    const execute = async (newName: string,nickname: string) => {
      setData("") 
      try {
        const response = await changeUserName(newName, nickname);
        setData(response);
      } catch (error) {
          setError(error)
      }

    }
    return {execute, data, error};
}

export default useChangeName;