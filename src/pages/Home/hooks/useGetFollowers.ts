import { useState } from "react";
import getUserFollowers from "../../../core/actions/Follow/GetUserFollowers";

const useGetFollowers = () => {
    const [data, setData] = useState<string[]>([]);

    const execute = async (nickname: string) => {
        const response = await getUserFollowers(nickname);
        setData(response);
    }
    return {execute, data};
}

export default useGetFollowers;