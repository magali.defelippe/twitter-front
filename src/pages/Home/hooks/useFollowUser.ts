import followUser from "../../../core/actions/Follow/FollowUser";
import { useState } from "react";

const useFollowUser = () => {
    const [data, setData] = useState("")
    const [error, setError] = useState("")
  
      const execute = async (follower: string, following: string) => {
        setData("") 
        try {
            const response = await followUser(follower, following);
            setData(response);
        } catch (error) {
            setError(error)
        }
  
      }
      return {execute, data, error};
}

export default useFollowUser;