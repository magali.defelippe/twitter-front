import UserInformation from "./components/UserInformation";
import Nav from "./components/Nav";
import WriteTweet from "./components/WriteTweet";
import TweetsContainer from "./components/TweetsContainer";
import SearchUsersContainer from "./components/SearchUsersContainer";
import AlertComponent from '../../components/AlertComponent'

import { Box, Grid, makeStyles } from "@material-ui/core";

const Home = () => {

  const useStyles = makeStyles((theme) => ({
    grid: {
      minHeight: "100vh",
      backgroundColor: "#fafafa",
    },
  }));

  const classes = useStyles();
  return (
    <div>
      <Nav />
      <Grid className={classes.grid} container direction="row" justify="center">
        <Grid item xs={2} sm={3}>
          <Box height="100%" boxShadow={1}>
            <UserInformation />
            <WriteTweet />
          </Box>
        </Grid>
        <Grid item xs={10} sm={6}>
          <Grid
            container
            style={{ display: "flex" }}
            direction="column"
            justify="flex-start"
            alignItems="center"
          >

            <Grid container direction="row" justify="center">
            <AlertComponent/>
              <TweetsContainer />
            </Grid>
          </Grid>
        </Grid>

        <Grid item sm={3}>
          <Box height="100%" boxShadow={1}>

            <SearchUsersContainer/>

          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;
