import { useEffect, useState } from "react";
import {useUserContext} from "../../../core/domain/User/UserContext";

import useGetTweets from "../hooks/useGetTweets";
import useCreateTweet from "../hooks/useCreateTweet";

import { useAlert } from "../../../state/AlertState";


export const useTweetState = () => {
  const {
    nickname,
    tweets,
    setTweets
  } = useUserContext();

  const { sendAlert} = useAlert();
  const [content, setContent] = useState('');
  const {execute: executeGetTweets, data: tweetsData} = useGetTweets();
  const {execute: executeCreateTweet, data: tweetData, error: errorTweet} = useCreateTweet();

  function getUserTweets(){
    executeGetTweets(nickname);
  }
  
  useEffect(()=>{
    setTweets(tweetsData.reverse())
  }, [tweetsData])

  function sendTweet(){
    executeCreateTweet(content, nickname)
  }

  useEffect(() => { 
    if(tweetData){
    sendAlert(tweetData)
    getUserTweets();
    setContent("")
    }
  }, [tweetData]);

  useEffect(() => {
    if(errorTweet){
    sendAlert(errorTweet, "error")
    }
  }, [errorTweet]);

  useEffect(() => {
    getUserTweets();
  }, []);

  return {
    sendTweet,
    setContent,
    content,
    tweets
  };
};
