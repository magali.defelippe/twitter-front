import { useEffect, useState } from "react";
import {useUserContext} from "../../../core/domain/User/UserContext";

import useGetUserInformation from "../hooks/useGetUserInformation";
import useGetFollowings from "../hooks/useGetFollowings";
import useGetFollowers from "../hooks/useGetFollowers";
import useChangeName from "../hooks/useChangeName";
import { useAlert } from "../../../state/AlertState";

export const useUserInformationState = () => {
  const {
    nickname,
    name,
    setName,
    followings,
    setFollowings,
    followers,
    setFollowers,
  } = useUserContext();
  const { sendAlert} = useAlert();
  const [follow, setFollow] = useState<string[]>([]);
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [edit, setEdit] = useState(false);
  const [newName, setNewName] = useState("");
  const {execute: executeInformation, data: dataInformation} = useGetUserInformation();
  const {execute: executeFollowings, data: dataFollowings} = useGetFollowings();
  const {execute: executeFollowers, data: dataFollowers} = useGetFollowers();
  const {execute: executeChangeName, data: dataChangeName, error: errorChangeName} = useChangeName();

  function getFollowings() {
    executeFollowings(nickname);
  }

  function getFollowers() {
    executeFollowers(nickname)
  }

   function getInformation() {
    executeInformation(nickname);
  }

  useEffect(() => {
    setName(dataInformation.name);
    setNewName(dataInformation.name);
  }, [dataInformation]);

  useEffect(() => {
    setFollowings(dataFollowings)
  }, [dataFollowings]);

  useEffect(() => {
    setFollowers(dataFollowers)
  }, [dataFollowers]);

  useEffect(() => { 
    if(dataChangeName){
    sendAlert(dataChangeName)
    setEdit(false)
    actualizeName()}
  }, [dataChangeName]);

  useEffect(() => {
    if(errorChangeName){
    sendAlert(errorChangeName, "error")
    setEdit(false)}
  }, [errorChangeName]);

  function loadUserInformation(){
    getInformation();
    getFollowers();
    getFollowings();
  }

  function changeNameByInput(value: string) {
    setNewName(value);
  }
  
  function setEditValue(value: boolean) {
    setEdit(value);
  }

  function openFollow(follow: string[], title: string) {
    setFollow(follow);
    setTitle(title);
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function changeName() {
    executeChangeName(newName, nickname);
  }

  function actualizeName(){
    setName(newName);
  }

  return {
    name,
    nickname,
    newName,
    follow,
    followings,
    followers,
    open,
    title,
    edit,
    openFollow,
    handleClose,
    changeName,
    changeNameByInput,
    setEditValue,
    loadUserInformation
  };
};
