import { useState, useEffect } from "react";
import {useUserContext} from "../../../core/domain/User/UserContext";
import { useAlert } from "../../../state/AlertState";

import useGetUserInformation from "../hooks/useGetUserInformation";
import useGetFollowings from "../hooks/useGetFollowings";
import useFollowUser from "../hooks/useFollowUser";

interface userFindInterface{
    name: string | any,
    nickname: string| any,
    follow: boolean
}
const initialState: userFindInterface = {name: '', nickname: '', follow: false};

export const useSearchState = () => {
  const {
    setFollowings,
    followings,
    nickname
  } = useUserContext();
  const { sendAlert} = useAlert();
  const [searchValue, setSearchValue] = useState("");
  const [userFind, setUser] = useState<userFindInterface>(initialState);
  const {execute: executeInformation, data: dataInformation} = useGetUserInformation();
  const {execute: executeFollowings, data: dataFollowings} = useGetFollowings();
  const {execute: executeFollow, data: dataFollow, error: errorFollow} = useFollowUser();

  function searchUser(){
    setUser(initialState);
    executeInformation(searchValue);
  }

  useEffect(() => {
    if(dataInformation){
      setUser({name: dataInformation.name, nickname: dataInformation.nickname, follow: checkIfIsFollowing()});
    }else{
      sendAlert("Usuario no encontrado", "error");
    }
  }, [dataInformation]);


  function checkIfIsFollowing(){
    const following = followings.find((follow: string) => follow === searchValue)
    if(following){return true}
    else{ return false}
  }

  function followAnUser(){
    executeFollow(nickname, userFind.nickname);
  }

  useEffect(() => {
    if(dataFollow){
      executeFollowings(nickname);
      setUser(initialState);
      sendAlert(dataFollow);
      setSearchValue("");
    }
  }, [dataFollow]);

  useEffect(() => {
    setFollowings(dataFollowings)
  }, [dataFollowings]);

  return {
    searchValue,
    setSearchValue,
    searchUser,
    userFind,
    followAnUser
  };
};
