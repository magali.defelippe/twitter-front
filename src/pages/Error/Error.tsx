import { Grid, Typography } from "@material-ui/core";

const Error = () => {
  return (
    <Grid
      style={{ minHeight: "100vh" }}
      container
      justify="center"
      alignContent="center"
    >
        <Grid container justify="center" item sm={12}>
        <Typography color="primary" variant="h1">Error</Typography>

        </Grid>

      <Typography>No tienes acceso</Typography>
    </Grid>
  );
};

export default Error;
