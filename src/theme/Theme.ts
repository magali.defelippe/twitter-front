import { createMuiTheme } from '@material-ui/core/styles';

 const theme = createMuiTheme({
	palette: {
	  primary: {
		main: '#ee303b',
	  },
	},
  });

export default theme;